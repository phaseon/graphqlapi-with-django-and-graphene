import graphene

from graphene_django import DjangoObjectType

from .models import UserCharacter


# TODO Create mutations

class UserCharacters(DjangoObjectType):
    class Meta:
        model = UserCharacter


class Query(object):
    character = graphene.Field(UserCharacters,
                               id=graphene.Int(),
                               name=graphene.String(),
                               level=graphene.Int(),
                               health=graphene.Int(),
                               creator=graphene.String(),
                               )

    all_characters = graphene.List(UserCharacters)

    def resolve_all_characters(self, info, **kwargs):

        return UserCharacter.objects.all()

    def resolve_character(self, info, **kwargs):
        id = kwargs.get('id')
        name = kwargs.get('name')

        if id is not None:
            return UserCharacter.objects.get(pk=id)

        if name is not None:
            return UserCharacter.objects.get(name=name)

        return None
