from django.db import models
from django.conf import settings
from django.core.validators import *

from character_properties.models import Ability, Effect, CharacterType


class UserCharacter(models.Model):

    creation_date = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __str__(self):  # Set string repr, easier time in admin view
        return self.name

    """
    Define character state
    """

    name = models.CharField(max_length=50)
    level = models.PositiveIntegerField(default=1, validators=[MinValueValidator(1), MaxValueValidator(150)])
    health = models.PositiveIntegerField(default=100)

    """
    Define character attributes
    """

    """
    type = models.ForeignKey(
        CharacterType,
        on_delete=models.CASCADE,
    )
    """

    creator = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    speed = models.PositiveIntegerField(default=1, validators=[MaxValueValidator(1000)])
    strength = models.PositiveIntegerField(default=1, validators=[MaxValueValidator(1000)])
    defense = models.PositiveIntegerField(default=1, validators=[MaxValueValidator(1000)])
    intelligence = models.PositiveIntegerField(default=1, validators=[MaxValueValidator(1000)])
    luck = models.PositiveIntegerField(default=1, validators=[MaxValueValidator(1000)])

    """
    Set abilities/effects
    """

    abilities = models.ManyToManyField(Ability)
    current_effects = models.ManyToManyField(Effect)
