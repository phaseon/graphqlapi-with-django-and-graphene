from django.apps import AppConfig


class UserCharactersConfig(AppConfig):
    name = 'user_characters'
