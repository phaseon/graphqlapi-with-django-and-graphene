from django.db import models
from django.contrib.auth.models import AbstractUser
from django_countries.fields import CountryField

from user_characters.models import UserCharacter


# Create your models here.

# TODO Extend user class


class User(AbstractUser):
    """
    Define extra field for user profile, extending django user model
    """

    birth_day = models.DateTimeField()

    city = models.CharField(max_length=100, blank=False)
    state = models.CharField(max_length=100, blank=False)
    country = CountryField(blank_label='(Select Country)')

    # Make sure USA is displayed first

    COUNTRIES_FIRST = [
        'US',
    ]

    REQUIRED_FIELDS = ['email', 'birth_day', 'city', 'state', 'country']

    characters_owned = models.ManyToManyField(UserCharacter, blank=True)
