from django.contrib import admin

from character_properties.models import *

# Register your models here.

admin.site.register(CharacterType)
admin.site.register(Effect)
admin.site.register(Ability)
