import graphene
from graphene_django import DjangoObjectType, DjangoConnectionField

from .models import CharacterType, Effect, Ability


# TODO Create mutations

class CharacterType(DjangoObjectType):
    class Meta:
        model = CharacterType


class EffectNode(DjangoObjectType):
    class Meta:
        model = Effect


class AbilityNode(DjangoObjectType):
    class Meta:
        model = Ability


class Query(object):
    all_character_types = graphene.List(CharacterType)
    all_effects = graphene.List(EffectNode)
    all_abilities = graphene.List(AbilityNode)

    def resolve_all_character_types(self, info, **kwargs):
        return CharacterType.objects.all()

    def resolve_all_effects(self, info, **kwargs):
        return Effect.objects.all()

    def resolve_all_abilities(self, info, **kwargs):
        return Ability.objects.all()
