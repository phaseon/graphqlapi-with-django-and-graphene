from django.apps import AppConfig


class CharacterPropertiesConfig(AppConfig):
    name = 'character_properties'
