from django.db import models
from django.core.validators import *


# TODO implement effects model
# TODO finish implementing abilities model
# TODO finish implementing character type (class) model


class CharacterType(models.Model):
    type_id = models.AutoField(primary_key=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=200, validators=[MinLengthValidator(4)])

    def __str__(self):  # Set string repr, easier time in admin view
        return self.name


class Effect(models.Model):
    effect_id = models.AutoField(primary_key=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=200, validators=[MinLengthValidator(3), MaxLengthValidator(200)])

    def __str__(self):  # Set string repr, easier time in admin view
        return self.name

    """
    Define effects
    """


class Ability(models.Model):
    ability_id = models.AutoField(primary_key=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    name = models.CharField(max_length=200, validators=[MinLengthValidator(4), MaxLengthValidator(200)])

    def __str__(self):  # Set string repr, easier time in admin view
        return self.name

    """
    Define abilities
    """

    attack = models.PositiveIntegerField(default=10, validators=[MaxValueValidator(250), MinValueValidator(10)])
    accuracy = models.PositiveIntegerField(default=10, validators=[MaxValueValidator(100), MinValueValidator(10)])
    PP = models.PositiveIntegerField(default=10, validators=[MaxValueValidator(56), MinValueValidator(10)])
    level = models.PositiveIntegerField(default=1, validators=[MaxValueValidator(150), MinValueValidator(1)])
    min_level = models.PositiveIntegerField(default=1, validators=[MaxValueValidator(150), MinValueValidator(1)])

    """
    Define effects for each ability
    """

    sets_effect = models.ManyToManyField(Effect)

    """
    Define what type of chars that can use ability
    """

    usable_by = models.ManyToManyField(CharacterType)
