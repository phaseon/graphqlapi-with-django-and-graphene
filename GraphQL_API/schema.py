import graphene

import character_properties.schema
import user_characters.schema


class Query(character_properties.schema.Query, user_characters.schema.Query, graphene.ObjectType):
    pass


schema = graphene.Schema(query=Query)
